package br.com.uniceplac.produtos.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.uniceplac.produtos.entity.Produtos;
import br.com.uniceplac.produtos.repository.ProdutoRepository;
import br.com.uniceplac.produtos.request.ProdutoRequest;
import br.com.uniceplac.produtos.service.ProdutoService;

@Controller
@RequestMapping(value = "/produtos")
public class ProdutosController {

	@Autowired
	private ProdutoRepository produtoRepository;

	@Autowired
	private ProdutoService produtoService;

	@RequestMapping(value = "/lista", method = RequestMethod.GET)
	public ModelAndView listarProdutos() {

		ModelAndView model = new ModelAndView("listaProdutos");

		List<Produtos> produtos = produtoRepository.findAll();

		return model.addObject("listProd", produtos);

	}

	@RequestMapping(value = "/salvar", method = RequestMethod.POST)
	public ModelAndView salvar(@ModelAttribute("produtoForm") @RequestBody ProdutoRequest model) {
		produtoService.cadastrarProduto(model);

		return new ModelAndView("redirect:/produtos/lista");
	}

	@RequestMapping(value = "/cadastrar/", method = RequestMethod.GET)
	public ModelAndView cadastraProduto() {
		ModelAndView model = new ModelAndView();

		ProdutoRequest cadastrarProduto = new ProdutoRequest();

		model.addObject("produtoForm", cadastrarProduto);
		model.setViewName("produtosForm");

		return model;

	}

	@RequestMapping(value = "/deletar/{id}", method = RequestMethod.GET)
	@Transactional
	public ModelAndView deletarProduto(@PathVariable Long id) {

//		Optional<Produtos> produto = produtoRepository.findById(id);
		

		produtoRepository.deleteById(id);

		return new ModelAndView("redirect:/produtos/lista");

	}

	@RequestMapping(value = "/editarproduto/{id}", method = RequestMethod.GET)
	public ModelAndView editarProduto(@ModelAttribute("produtoForm") @PathVariable Long id) {

		ModelAndView modelAndView = new ModelAndView();
		
		Produtos produto = produtoRepository.getOne(id);
		
		modelAndView.addObject("produtoForm", produto);
		modelAndView.setViewName("produtosForm");

		return new ModelAndView("redirect:/produtos/lista");
	}

}
