package br.com.uniceplac.produtos.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.uniceplac.produtos.Response.ProdutosResponse;
import br.com.uniceplac.produtos.entity.Produtos;
import br.com.uniceplac.produtos.repository.ProdutoRepository;
import br.com.uniceplac.produtos.request.ProdutoRequest;

@Service
public class ProdutoService {

	@Autowired
	public ProdutoRepository produtoRepository;

	public ProdutosResponse cadastrarProduto(ProdutoRequest model) {

		Produtos produto = model.converter();

		produtoRepository.save(produto);
		
		return new ProdutosResponse(produto);

	}


}
