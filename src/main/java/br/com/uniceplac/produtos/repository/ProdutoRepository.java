package br.com.uniceplac.produtos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.uniceplac.produtos.entity.Produtos;

public interface ProdutoRepository extends JpaRepository<Produtos, Long>{
	

}
