package br.com.uniceplac.produtos.Response;

import java.util.List;
import java.util.stream.Collectors;

import br.com.uniceplac.produtos.entity.Produtos;

public class ProdutosResponse {
	
	private Long idProduto;
	
	private String nome;
	
	private String tipo;
	
	public ProdutosResponse(Produtos produto) {
		this.idProduto = produto.getIdProduto();
		this.nome = produto.getNome();
		this.tipo = produto.getTipo();
	}

	
	public Long getIdProduto() {
		return idProduto;
	}


	public void setIdProduto(Long idProduto) {
		this.idProduto = idProduto;
	}


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
    public static List<ProdutosResponse> converter(List<Produtos> produtos) {
		
		return produtos.stream().map(ProdutosResponse::new).collect(Collectors.toList());
	}

}
