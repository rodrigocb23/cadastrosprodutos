package br.com.uniceplac.produtos.request;

import br.com.uniceplac.produtos.entity.Produtos;

public class ProdutoRequest {
	
	private Long idProduto;
	
	private String nome;
	
	private String tipo;
	
	
	public Produtos converter() {		
		return new Produtos(idProduto,nome, tipo);
	}
	
	public Long getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(Long idProduto) {
		this.idProduto = idProduto;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	

}
